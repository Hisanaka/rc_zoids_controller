package jp.or.ixqsware.rc_zoids_controller.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import jp.or.ixqsware.rc_zoids_controller.R;

import static jp.or.ixqsware.rc_zoids_controller.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.rc_zoids_controller.Constants.PREFS_NAME;
import static jp.or.ixqsware.rc_zoids_controller.Constants.PREF_LEFT;
import static jp.or.ixqsware.rc_zoids_controller.Constants.PREF_RIGHT;

/**
 * Ver.3 Fragmentの左右コントロールの下限値を設定する
 *
 * Created by hnakadate on 16/02/13.
 */
public class AdjustmentDialog extends DialogFragment {

    public static AdjustmentDialog newInstance(int sectionNumber) {
        AdjustmentDialog instance = new AdjustmentDialog();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        instance.setArguments(args);
        return instance;
    }

    public AdjustmentDialog() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_adjustment, container, false);

        SharedPreferences preferences
                = getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int valueRight = preferences.getInt(PREF_RIGHT, 50);
        int valueLeft  = preferences.getInt(PREF_LEFT, 50);

        final TextView textRight = (TextView) rootView.findViewById(R.id.right_value);
        final TextView textLeft  = (TextView) rootView.findViewById(R.id.left_value);

        SeekBar seekRight = (SeekBar) rootView.findViewById(R.id.right_threshold);
        SeekBar seekLeft  = (SeekBar) rootView.findViewById(R.id.left_threshold);

        seekRight.setProgress(valueRight);
        seekLeft.setProgress(valueLeft);

        textRight.setText(getString(R.string.threshold_value, valueRight));
        textLeft.setText(getString(R.string.threshold_value, valueLeft));

        seekRight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textRight.setText(getString(R.string.threshold_value, progress));
                textRight.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                saveThreshold(PREF_RIGHT, seekBar.getProgress());
            }
        });

        seekLeft.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textLeft.setText(getString(R.string.threshold_value, progress));
                textLeft.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                saveThreshold(PREF_LEFT, seekBar.getProgress());
            }
        });

        Button  buttonOK  = (Button) rootView.findViewById(R.id.ok_button);
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setHasOptionsMenu(false);

        return rootView;
    }

    private void saveThreshold(String key, int value) {
        SharedPreferences preferences
                = getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }
}
