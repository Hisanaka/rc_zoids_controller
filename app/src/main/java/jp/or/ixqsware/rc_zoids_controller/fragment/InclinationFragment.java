package jp.or.ixqsware.rc_zoids_controller.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import jp.or.ixqsware.rc_zoids_controller.R;

import static jp.or.ixqsware.rc_zoids_controller.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_VER3;

/**
 * 端末の傾きにより操作を行う際に使用するフラグメント(F/W Ver.3.x用)
 *
 * Created by hisanaka on 16/01/05.
 */
public class InclinationFragment extends BaseFragment {
    private static final double RAD2DEG = 180 / Math.PI;

    private TextView textPitch;
    private TextView textRoll;
    private boolean onGo = false;

    public static InclinationFragment newInstance(int sectionNumber) {
        InclinationFragment instance = new InclinationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        instance.setArguments(args);
        return instance;
    }

    public InclinationFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inclination, container, false);

        textPitch = (TextView) rootView.findViewById(R.id.pitch_text);
        textRoll  = (TextView) rootView.findViewById(R.id.roll_text);

        textPitch.setText(getString(R.string.fb_label, "", 0));
        textRoll.setText(getString(R.string.lr_label, "", 0));

        Button actionButton = (Button) rootView.findViewById(R.id.action_button);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                button.setSelected(!button.isSelected());
                if (button.isSelected()) {
                    button.setText(getString(R.string.start_label));
                } else {
                    button.setText(getString(R.string.stop_label));
                    if (mListener != null) {
                        mListener.writeCharacteristic(new int[]{128, 128});
                    }
                }
                onGo = button.isSelected();
            }
        });

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_propo);
        item.setEnabled(mListener != null);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_inclination, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_propo:
                if (mListener != null) {
                    mListener.switchFragment(SECTION_VER3);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setValue(float[] values) {
        if (!onGo) { return; }

        int pitch = (int) (values[1] * RAD2DEG);
        int roll  = (int) (values[2] * RAD2DEG);

        // pitch : 左右方向(電源ボタンを上にした状態で45(左方向) ~ -45(右方向)の範囲を有効とする)
        // roll  : 前後方向(電源ボタンを上にした状態で0(前進) ~ -120(後退)の範囲を有効とする)
        int frontAndBack = roll + 120;
        int leftAndRight = pitch + 45;

        if (frontAndBack < 0) {
            frontAndBack = 0;
        } else if (frontAndBack > 120) {
            frontAndBack = 120;
        }

        if (leftAndRight < 0) {
            leftAndRight = 0;
        } else if (leftAndRight > 90) {
            leftAndRight = 90;
        }

        frontAndBack = (int) Math.ceil(frontAndBack * (255.0d / 120.0d));
        leftAndRight = (int) Math.ceil(leftAndRight * (255.0d / 90.0d));

        String lrLabel = getString(R.string.lr_label, "", 0);
        if (leftAndRight > 128) {
            lrLabel = getString(R.string.lr_label,
                    getString(R.string.right_label), leftAndRight - 128);

        } else if (leftAndRight < 128) {
            lrLabel = getString(R.string.lr_label,
                    getString(R.string.left_label), Math.abs(leftAndRight - 128));
        }

        String fbLabel = getString(R.string.fb_label, "", 0);
        if (frontAndBack > 128) {
            fbLabel = getString(R.string.fb_label,
                    getString(R.string.front_label), frontAndBack - 128);

        } else if (frontAndBack < 128) {

            fbLabel = getString(R.string.fb_label,
                    getString(R.string.back_label), Math.abs(frontAndBack - 128));
        }

        textRoll.setText(lrLabel);
        textPitch.setText(fbLabel);

        if (mListener != null) {
            mListener.writeCharacteristic(new int[] {frontAndBack, leftAndRight});
        }
    }
}
