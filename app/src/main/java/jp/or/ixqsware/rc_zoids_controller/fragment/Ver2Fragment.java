package jp.or.ixqsware.rc_zoids_controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;

import jp.or.ixqsware.rc_zoids_controller.R;

import static jp.or.ixqsware.rc_zoids_controller.Constants.ARG_SECTION_NUMBER;

/**
 * 8方向のボタンによる操作を行うためのフラグメント(F/W Ver.2.x用)
 *
 * Created by hnakadate on 15/11/05.
 */
public class Ver2Fragment extends BaseFragment {

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener == null) { return; }

            int charValue = 0;
            switch (v.getId()) {
                case R.id.button00:
                    break;

                case R.id.button01:
                    charValue = 1;
                    break;
                case R.id.button02:
                    charValue = 2;
                    break;

                case R.id.button03:
                    charValue = 3;
                    break;

                case R.id.button04:
                    charValue = 4;
                    break;

                case R.id.button05:
                    charValue = 5;
                    break;

                case R.id.button06:
                    charValue = 6;
                    break;

                case R.id.button07:
                    charValue = 7;
                    break;

                case R.id.button08:
                    charValue = 8;
                    break;

                case R.id.button09:
                    charValue = 9;
                    break;

                case R.id.button10:
                    charValue = 10;
                    break;
            }
            mListener.writeCharacteristic(charValue);
        }
    };

    public static Ver2Fragment newInstance(int sectionNumber) {
        Ver2Fragment instance = new Ver2Fragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        instance.setArguments(args);
        return instance;
    }

    public Ver2Fragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseFragment.FragmentCallbackListener) {
            mListener = (BaseFragment.FragmentCallbackListener) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_control2, container, false);

        ArrayList<View> arrButtons = new ArrayList<>();
        rootView.findViewsWithText(arrButtons, getString(R.string.control_tag), View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);

        for (View v : arrButtons) {
            ImageButton button = (ImageButton) v;

            button.setOnClickListener(clickListener);

            int multiplier = 0;
            switch (button.getId()) {
                case R.id.button00:
                    continue;

                case R.id.button01:
                    multiplier = 2;
                    break;

                case R.id.button02:
                    multiplier = 3;
                    break;

                case R.id.button03:
                    multiplier = 4;
                    break;

                case R.id.button04:
                    multiplier = 5;
                    break;

                case R.id.button05:
                    multiplier = 6;
                    break;

                case R.id.button06:
                    multiplier = 7;
                    break;

                case R.id.button07:
                    multiplier = 0;
                    break;

                case R.id.button08:
                    multiplier = 1;
                    break;

                case R.id.button09:
                    button.setScaleX(-1.0f);
                    continue;

                case R.id.button10:
                    continue;
            }

            button.setRotation(45.0f * multiplier);
        }

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mListener != null) {
            mListener.disconnectDevice();
        }
    }

}
