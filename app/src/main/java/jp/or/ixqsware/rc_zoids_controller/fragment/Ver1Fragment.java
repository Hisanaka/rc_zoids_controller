package jp.or.ixqsware.rc_zoids_controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import jp.or.ixqsware.rc_zoids_controller.view.CustomSeekBar;
import jp.or.ixqsware.rc_zoids_controller.R;

import static jp.or.ixqsware.rc_zoids_controller.Constants.ARG_SECTION_NUMBER;

/**
 * シークバーにより前進・後退のみを行うためのフラグメント(F/W Ver.1.x用)
 *
 * Created by hisanake on 15/09/25.
 */
public class Ver1Fragment extends BaseFragment implements SeekBar.OnSeekBarChangeListener {

    public static Ver1Fragment newInstance(int sectionNumber) {
        Ver1Fragment instance = new Ver1Fragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        instance.setArguments(args);
        return instance;
    }

    public Ver1Fragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallbackListener) {
            mListener = (FragmentCallbackListener) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_control1, container, false);

        CustomSeekBar controlBar = (CustomSeekBar) rootView.findViewById(R.id.control_bar);
        controlBar.setMax(510);
        controlBar.setProgress(255);
        controlBar.setOnSeekBarChangeListener(this);

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mListener != null) {
            mListener.disconnectDevice();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (mListener != null) {
            int mDirection = progress - 255;
            if (mDirection < 0) {
                mListener.writeCharacteristic(new int[] {1, Math.abs(mDirection)});
            } else {
                mListener.writeCharacteristic(new int[] {0, mDirection});
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        seekBar.setProgress(255);
    }
}
