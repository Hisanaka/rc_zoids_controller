package jp.or.ixqsware.rc_zoids_controller;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.or.ixqsware.rc_zoids_controller.dialog.AnnounceDialog;
import jp.or.ixqsware.rc_zoids_controller.fragment.BaseFragment;
import jp.or.ixqsware.rc_zoids_controller.fragment.DetectFragment;
import jp.or.ixqsware.rc_zoids_controller.fragment.InclinationFragment;
import jp.or.ixqsware.rc_zoids_controller.fragment.Ver1Fragment;
import jp.or.ixqsware.rc_zoids_controller.fragment.Ver2Fragment;
import jp.or.ixqsware.rc_zoids_controller.fragment.Ver3Fragment;

import static jp.or.ixqsware.rc_zoids_controller.Constants.ARG_BLUETOOTH_DEVICE;
import static jp.or.ixqsware.rc_zoids_controller.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.rc_zoids_controller.Constants.REQUEST_ENABLE_BLUETOOTH;
import static jp.or.ixqsware.rc_zoids_controller.Constants.REQUEST_LOCATION_MODE_ENABLE;
import static jp.or.ixqsware.rc_zoids_controller.Constants.REQUEST_LOCATION_PERMISSION;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SCAN_PERIOD;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_CONTROLLER;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_DETECT;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_VER1;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_VER2;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_VER3;
import static jp.or.ixqsware.rc_zoids_controller.Constants.TAG_ANNOUNCE;
import static jp.or.ixqsware.rc_zoids_controller.Constants.TAG_CONTROL;
import static jp.or.ixqsware.rc_zoids_controller.Constants.TAG_DETECT;
import static jp.or.ixqsware.rc_zoids_controller.Constants.TAG_OPERATION;
import static jp.or.ixqsware.rc_zoids_controller.Constants.UUID_DEVICE_INFORMATION_SERVICE;
import static jp.or.ixqsware.rc_zoids_controller.Constants.UUID_FIRMWARE_VERSION;
import static jp.or.ixqsware.rc_zoids_controller.Constants.UUID_GPIO_OPERATION_SERVICE;
import static jp.or.ixqsware.rc_zoids_controller.Constants.UUID_WRITE_ACCEL_CHARACTERISTIC;

public class MainActivity extends AppCompatActivity
            implements BaseFragment.FragmentCallbackListener {
    private static final Pattern PATTERN_FW_VER = Pattern.compile("(\\d+)\\..*");
    private static final int WRITE_CHAR_INTERVAL_MILLIS = 150;

    private FrameLayout frameLayout;
    private int currentFragment = SECTION_DETECT;
    private ProgressDialog progressDialog = null;

    private ArrayList<BluetoothDevice> arrDevices = new ArrayList<>();
    private BluetoothGatt mBtGatt;
    private BluetoothLeScanner mBtLeScanner;
    private BluetoothDevice bleDevice;

    private long lastWriteCharTimeInMillis = 0L;
    private boolean isStop = false;

    private final ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            BluetoothDevice device = result.getDevice();
            if (!arrDevices.contains(device)) { arrDevices.add(device); }
            super.onScanResult(callbackType, result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> result) {
            super.onBatchScanResults(result);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    gatt.discoverServices();

                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Snackbar.make(
                            frameLayout,
                            getString(R.string.disconnect_label),
                            Snackbar.LENGTH_SHORT
                    ).show();
                    setTitle(getString(R.string.app_name));
                    showFragment(SECTION_DETECT);
                }
            } else {
                /* status 133対策(Documentに記載がなく、詳細不明/連続して接続すると発生) */
                Snackbar.make(frameLayout, R.string.congest_label, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok_label, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                // とりあえず何もしない(ユーザーからのレスポンス待ちで時間稼ぎ)
                                }
                        })
                        .setActionTextColor(
                                ContextCompat.getColor(getApplicationContext(), R.color.snackbar_action))
                        .show();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status != BluetoothGatt.GATT_SUCCESS){
                Snackbar.make(frameLayout, R.string.service_detection_fail, Snackbar.LENGTH_SHORT)
                        .show();
                setTitle(getString(R.string.app_name));

            } else {
                boolean isZoids = false;
                for (BluetoothGattService service : gatt.getServices()) {
                    UUID uuid = service.getUuid();
                    if (uuid.equals(UUID.fromString(UUID_GPIO_OPERATION_SERVICE))) {
                        isZoids = true;
                        break;
                    }
                }

                if (isZoids) {
                    mBtGatt = gatt;
                    BluetoothGattCharacteristic charFirmwareVer
                            = mBtGatt.getService(UUID.fromString(UUID_DEVICE_INFORMATION_SERVICE))
                            .getCharacteristic(UUID.fromString(UUID_FIRMWARE_VERSION));
                    if (!mBtGatt.readCharacteristic(charFirmwareVer)) {
                        Snackbar.make(frameLayout, R.string.service_detection_fail, Snackbar.LENGTH_SHORT)
                                .show();
                    }
                } else {
                    Snackbar.make(frameLayout, getString(R.string.wrong_device), Snackbar.LENGTH_SHORT)
                            .show();
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                Snackbar.make(frameLayout, R.string.service_detection_fail, Snackbar.LENGTH_SHORT)
                        .show();
                setTitle(getString(R.string.app_name));
                return;
            }

            String verFW = characteristic.getStringValue(0);
            Matcher matcher = PATTERN_FW_VER.matcher(verFW);
            if (matcher.find()) {
                int sectionNumber = Integer.parseInt(matcher.group(1));
                showFragment(sectionNumber);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (isStop || status == BluetoothGatt.GATT_SUCCESS) {
                isStop = true;
            } else {
                // 停止するまで繰り返し書き込む
                gatt.writeCharacteristic(characteristic);
            }
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }
    };

    private SensorManager sensorManager;
    private float[] geoMagnetics   = new float[3];
    private float[] gravities      = new float[3];
    private float[] rotationMatrix = new float[9];
    private float[] attitude       = new float[3];
    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_MAGNETIC_FIELD:  // 磁気センサー
                    geoMagnetics = event.values.clone();
                    break;

                case Sensor.TYPE_ACCELEROMETER:  // 加速度センサー
                    gravities = event.values.clone();
                    break;
            }

            // 磁気センサーと加速度センサーから傾きを取得
            SensorManager.getRotationMatrix(rotationMatrix, null, gravities, geoMagnetics);
            SensorManager.getOrientation(rotationMatrix, attitude);

            InclinationFragment fragment
                    = (InclinationFragment) getSupportFragmentManager().findFragmentByTag(TAG_CONTROL);
            if (fragment != null) {
                fragment.setValue(attitude);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bleDevice = null;
        if (savedInstanceState != null) {
            currentFragment = savedInstanceState.getInt(ARG_SECTION_NUMBER);
            bleDevice = savedInstanceState.getParcelable(ARG_BLUETOOTH_DEVICE);
        }
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        frameLayout = (FrameLayout) findViewById(R.id.container);

        BluetoothManager mBtManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (mBtManager != null) {
            BluetoothAdapter mBtAdapter = mBtManager.getAdapter();
            if (mBtAdapter != null) {
                mBtLeScanner = mBtAdapter.getBluetoothLeScanner();
                if (!mBtAdapter.isEnabled()) {
                    Intent intentBtOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intentBtOn, REQUEST_ENABLE_BLUETOOTH);
                }
            } else {
                Snackbar.make(frameLayout, R.string.unsupported_bluetooth, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok_label, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        })
                        .show();
            }
        } else {
            Snackbar.make(frameLayout, R.string.unsupported_bluetooth, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok_label, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bleDevice == null) {
            showFragment(currentFragment);
        } else {
            connectToDevice(bleDevice);
        }
    }

    @Override
    protected void onDestroy() {
        if (mBtGatt != null) {
            mBtGatt.close();
            mBtGatt = null;
        }
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ARG_SECTION_NUMBER, currentFragment);
        outState.putParcelable(ARG_BLUETOOTH_DEVICE, bleDevice);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent date) {
        super.onActivityResult(requestCode, resultCode, date);
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                BluetoothManager mBtManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                BluetoothAdapter mBtAdapter = mBtManager.getAdapter();
                mBtLeScanner = mBtAdapter.getBluetoothLeScanner();
                showFragment(currentFragment);
            } else {
                finish();
            }
        }
    }

    private void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    private void showFragment(int sectionNumber) {
        switch (sectionNumber) {
            case SECTION_DETECT:
                setTitle(getString(R.string.app_name));
                sensorManager.unregisterListener(sensorEventListener);
                showDetectFragment();
                break;

            case SECTION_VER1:
                sensorManager.unregisterListener(sensorEventListener);
                showVer1Fragment();
                break;

            case SECTION_VER2:
                sensorManager.unregisterListener(sensorEventListener);
                showVer2Fragment();
                break;

            case SECTION_VER3:
                sensorManager.unregisterListener(sensorEventListener);
                showVer3Fragment();
                break;

            case SECTION_CONTROLLER:
                showControllerFragment();
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResult) {
        if (requestCode != REQUEST_LOCATION_PERMISSION) { return; }

        if (grantResult[0] == PackageManager.PERMISSION_GRANTED) {
            doScan();
        } else {
            Snackbar.make(frameLayout, getString(R.string.permission), Snackbar.LENGTH_LONG)
                    .setActionTextColor(getColor(R.color.snackbar_action))
                    .setAction(getString(R.string.retry_label), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestLocationPermissions();
                        }
                    })
                    .show();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestLocationPermissions() {
        requestPermissions(
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_LOCATION_PERMISSION
        );
    }

    @Override
    public void scanDevices() {
        if (mBtLeScanner == null) { return; }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            doScan();

        } else {
            if (!isLocationModeEnabled()) {
                AnnounceDialog dialog = AnnounceDialog.newInstance(null,
                        REQUEST_LOCATION_MODE_ENABLE, getString(R.string.enable_location_title),
                        getString(R.string.enable_location));
                dialog.show(getSupportFragmentManager(), TAG_ANNOUNCE);
                return;
            }
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestLocationPermissions();

            } else {
                doScan();
            }
        }
    }

    private boolean isLocationModeEnabled() {
        LocationManager locationManager
                = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null
                && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void doScan() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.searching_label));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);
        progressDialog.show();

        Handler stopHandler = new Handler();
        stopHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBtLeScanner.stopScan(leScanCallback);
                /* スキャンに失敗した場合にペアリング済みのデバイス一覧を表示する
                if (arrDevices.size() == 0) {
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    Set<BluetoothDevice> deviceSet = bluetoothAdapter.getBondedDevices();
                    arrDevices = new ArrayList<>(deviceSet);
                }*/

                FragmentManager fragmentManager = getSupportFragmentManager();
                DetectFragment fragment
                        = (DetectFragment) fragmentManager.findFragmentByTag(TAG_DETECT);
                if (fragment != null && fragment.isVisible()) {
                    fragment.updateLists(arrDevices);
                }
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                progressDialog = null;
            }
        }, SCAN_PERIOD);

        mBtLeScanner.stopScan(leScanCallback);
        mBtLeScanner.startScan(leScanCallback);
    }

    @Override
    public void connectToDevice(BluetoothDevice device) {
        setTitle(device.getName());
        bleDevice = device;
        mBtGatt = device.connectGatt(this, false, mGattCallback);
    }

    @Override
    public void disconnectDevice() {
        //mBtGatt.disconnect();
    }

    @Override
    public void writeCharacteristic(int[] charValues) {
        if (mBtGatt == null) { return; }

        // 停止の書き込みに失敗した場合は成功するまで繰り返すので、そのためのフラグを立てる
        isStop = charValues[0] != 127;

        // 連続して書き込むとエラーになるのでしばらく待つ。ただし、停止に関しては書き込みを続ける。
        Calendar calendar = Calendar.getInstance();
        if (calendar.getTimeInMillis() - lastWriteCharTimeInMillis < WRITE_CHAR_INTERVAL_MILLIS
                && isStop) {
            return;
        }
        lastWriteCharTimeInMillis = calendar.getTimeInMillis();

        BluetoothGattService mBtGattService
                = mBtGatt.getService(UUID.fromString(UUID_GPIO_OPERATION_SERVICE));
        if (mBtGattService == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.service)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }

        UUID uuid = UUID.fromString(UUID_WRITE_ACCEL_CHARACTERISTIC);
        BluetoothGattCharacteristic mBtChar = mBtGattService.getCharacteristic(uuid);
        if (mBtChar == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.characteristic)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }

        // Characteristicの書き込み
        byte[] bytes = new byte[2];
        bytes[0] = (byte) charValues[0];
        bytes[1] = (byte) charValues[1];
        mBtChar.setValue(bytes);
        mBtChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mBtGatt.writeCharacteristic(mBtChar);
    }

    @Override
    public void writeCharacteristic(int charValues) {
        // 停止の書き込みに失敗した場合は成功するまで繰り返すので、そのためのフラグを立てる
        isStop = charValues != 0;

        // 連続して書き込むとエラーになるのでしばらく待つ。ただし、停止に関しては書き込みを続ける。
        Calendar calendar = Calendar.getInstance();
        if (calendar.getTimeInMillis() - lastWriteCharTimeInMillis < WRITE_CHAR_INTERVAL_MILLIS
                && isStop) {
            return;
        }
        lastWriteCharTimeInMillis = calendar.getTimeInMillis();

        BluetoothGattService mBtGattService
                = mBtGatt.getService(UUID.fromString(UUID_GPIO_OPERATION_SERVICE));
        if (mBtGattService == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.service)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }

        UUID uuid = UUID.fromString(UUID_WRITE_ACCEL_CHARACTERISTIC);
        BluetoothGattCharacteristic mBtChar = mBtGattService.getCharacteristic(uuid);
        if (mBtChar == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.characteristic)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }

        // Characteristicの書き込み
        byte[] bytes = new byte[1];
        bytes[0] = (byte) charValues;
        mBtChar.setValue(bytes);
        mBtChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mBtGatt.writeCharacteristic(mBtChar);
    }

    @Override
    public void switchFragment(int sectionNumber) {
        showFragment(sectionNumber);
    }

    private void setFragment(Fragment fragment, String tag, boolean addStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment, tag);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (addStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private void showDetectFragment() {
        currentFragment = SECTION_DETECT;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        DetectFragment fragment = DetectFragment.newInstance(currentFragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        fragment.setArguments(args);

        setFragment(fragment, TAG_DETECT, false);
    }

    private void showVer1Fragment() {
        currentFragment = SECTION_VER1;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        Ver1Fragment fragment = Ver1Fragment.newInstance(currentFragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        fragment.setArguments(args);

        setFragment(fragment, TAG_OPERATION, true);
    }

    private void showVer2Fragment() {
        currentFragment = SECTION_VER2;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        Ver2Fragment fragment = Ver2Fragment.newInstance(currentFragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        fragment.setArguments(args);

        setFragment(fragment, TAG_OPERATION, true);
    }

    private void showVer3Fragment() {
        boolean addStack = currentFragment != SECTION_CONTROLLER;
        currentFragment = SECTION_VER3;

        Configuration configuration = getResources().getConfiguration();
        int screenOrientation = configuration.orientation;
        if (screenOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        } else {
            Ver3Fragment fragment = Ver3Fragment.newInstance(currentFragment);

            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, currentFragment);
            fragment.setArguments(args);

            setFragment(fragment, TAG_OPERATION, addStack);
        }
    }

    private void showControllerFragment() {
        currentFragment = SECTION_CONTROLLER;

        sensorManager.registerListener(
                sensorEventListener,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL
        );
        sensorManager.registerListener(
                sensorEventListener,
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL
        );

        InclinationFragment fragment = InclinationFragment.newInstance(currentFragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        fragment.setArguments(args);

        setFragment(fragment, TAG_CONTROL, false);
    }

}