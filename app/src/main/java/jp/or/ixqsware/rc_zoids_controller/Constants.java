package jp.or.ixqsware.rc_zoids_controller;

/**
 * Created by hnakadate on 15/09/14.
 */
public class Constants {
    public static final String ARG_SECTION_NUMBER   = "section_number";
    public static final String ARG_BLUETOOTH_DEVICE = "bluetooth_device";
    public static final String ARG_TITLE            = "title";
    public static final String ARG_MESSAGE          = "message";
    public static final String ARG_EXTRA            = "extra";

    public static final String TAG = "RC_ZOIDS_DEBUG";

    public static final int REQUEST_ENABLE_BLUETOOTH     = 0;
    public static final int REQUEST_LOCATION_PERMISSION  = 1;
    public static final int REQUEST_LOCATION_MODE_ENABLE = 2;

    public static final String TAG_DETECT    = "detect";
    public static final String TAG_OPERATION = "operation";
    public static final String TAG_CONTROL   = "control";
    public static final String TAG_ANNOUNCE  = "announce";
    public static final String TAG_ADJUST    = "adjustment";

    public static final String PREFS_NAME = "rc_zoids_pref";
    public static final String PREF_RIGHT = "right_threshold";
    public static final String PREF_LEFT  = "left_threshold";

    public static final int SECTION_DETECT     = 0;
    public static final int SECTION_VER1       = 1;
    public static final int SECTION_VER2       = 2;
    public static final int SECTION_VER3       = 3;
    public static final int SECTION_CONTROLLER = 4;
    public static final int SECTION_ADJUSTMENT = 5;

    public static final long SCAN_PERIOD = 5000;

    public static final String UUID_DEVICE_INFORMATION_SERVICE
            = "0000180a-0000-1000-8000-00805f9b34fb";

    public static final String UUID_FIRMWARE_VERSION
            = "00002a26-0000-1000-8000-00805f9b34fb";

    public static final String UUID_GPIO_OPERATION_SERVICE
            = "4e76f100-4f13-4605-b976-f7cd9c14af82";

    public static final String UUID_WRITE_ACCEL_CHARACTERISTIC
            = "4e76f101-4f13-4605-b976-f7cd9c14af82";
}
