package jp.or.ixqsware.rc_zoids_controller.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import jp.or.ixqsware.rc_zoids_controller.R;
import jp.or.ixqsware.rc_zoids_controller.dialog.AdjustmentDialog;
import jp.or.ixqsware.rc_zoids_controller.view.CustomSeekBar;

import static jp.or.ixqsware.rc_zoids_controller.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.rc_zoids_controller.Constants.PREFS_NAME;
import static jp.or.ixqsware.rc_zoids_controller.Constants.PREF_LEFT;
import static jp.or.ixqsware.rc_zoids_controller.Constants.PREF_RIGHT;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_ADJUSTMENT;
import static jp.or.ixqsware.rc_zoids_controller.Constants.SECTION_CONTROLLER;
import static jp.or.ixqsware.rc_zoids_controller.Constants.TAG_ADJUST;

/**
 * シークバーにより前後左右に操作するためのフラグメント(F/W Ver.3.x用)
 *
 * Created by hnakadate on 16/01/02.
 */
public class Ver3Fragment extends BaseFragment {

    public static Ver3Fragment newInstance(int sectionNumber) {
        Ver3Fragment instance = new Ver3Fragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        instance.setArguments(args);
        return instance;
    }

    public Ver3Fragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_control3, container, false);

        final CustomSeekBar seekFrontBack
                = (CustomSeekBar) rootView.findViewById(R.id.front_back_bar);
        final CustomSeekBar seekLeftRight
                = (CustomSeekBar) rootView.findViewById(R.id.left_right_bar);

        seekFrontBack.setMax(255);
        seekFrontBack.setProgress(128);

        seekLeftRight.setMax(255);
        seekLeftRight.setProgress(128);

        seekFrontBack.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mListener != null) {
                    int leftAndRight = getProgress(seekLeftRight.getProgress());
                    mListener.writeCharacteristic(new int[] {progress, leftAndRight});
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setProgress(128);
            }
        });

        seekLeftRight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mListener != null) {
                    int frontAndBack = seekFrontBack.getProgress();
                    int leftAndRight = getProgress(progress);
                    mListener.writeCharacteristic(new int[] {frontAndBack, leftAndRight});
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setProgress(128);
            }
        });

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mListener != null) {
            mListener.disconnectDevice();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_inclination);
        item.setEnabled(mListener != null);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_ver3, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_inclination:
                if (mListener != null) {
                    mListener.switchFragment(SECTION_CONTROLLER);
                }
                break;

            case R.id.action_adjustment:
                AdjustmentDialog dialog = AdjustmentDialog.newInstance(SECTION_ADJUSTMENT);
                dialog.show(getActivity().getSupportFragmentManager(), TAG_ADJUST);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 左右方向の回転量を一定範囲に制限する
     * @param progress ProgressBarの値
     * @return 補正後の値
     */
    int getProgress(int progress) {
        SharedPreferences preferences
                = getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int minRight = preferences.getInt(PREF_RIGHT, 50);
        int minLeft  = 255 - preferences.getInt(PREF_LEFT, 50);
        if (progress < minRight) {        // 右
            return minRight;
        } else if (progress > minLeft) {  // 左
            return minLeft;
        } else {
            return progress;
        }
    }
}
